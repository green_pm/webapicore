﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace FileExplorer
{
    public class FileOrFolder
    {
        public string Name      { get; set; } = "" ;
        public string FullName  { get; set; } = "" ;
        public string Extension { get; set; } = "" ;
        public string Icon      { get; set; } = "" ;

        public bool?  IsFolder  { get; set; }

        public ulong? Size      { get; set; }
    }

    public class FilesAndFolders
    {
        public List<FileOrFolder> Files { get; private set; }
        public List<FileOrFolder> Folders { get; private set; }
        public List<FileOrFolder> SummaryList
        {
            get
            {
                var res = new List<FileOrFolder>();
                res.AddRange(Folders);
                res.AddRange(Files);
                return res;
            }
        }

        private static Size _size = new Size(32, 32);

        public static FilesAndFolders Get(List<FileOrFolder> files, List<FileOrFolder> folders)
        {
            return new FilesAndFolders() { Files = files, Folders = folders };
        }

        public static FileOrFolder GetFolder(string fullName, Size? size = null)
        {
            size = size ?? _size;

            var inf = new DirectoryInfo(fullName);

            try
            {
                // ни одной рабочей проверки доступа к папкам не нашел,
                // потому проверяем таким костыльным стособом
                // вся проблема была с папкой System Volume Information
                inf.GetFileSystemInfos();
            }
            catch (Exception)
            {
                return null;
            }

            var faf = new FileOrFolder
            {
                Name = inf.Name,
                FullName = inf.FullName,
                IsFolder = true,
            };

            // Отключено для отдельного получения дерева файлов и их картинок
            //if (size.Value.Width <= 16 && size.Value.Height <= 16)
            //{
            //    var icon = ShellIcon.GetSmallFolderIcon(faf.FullName);
            //    faf.Icon = ConvertIconToString(icon);
            //}
            //else if (size.Value.Width <= 32 && size.Value.Height <= 32)
            //{
            //    var icon = ShellIcon.GetLargeFolderIcon(faf.FullName);
            //    faf.Icon = ConvertIconToString(icon);
            //}
            //else
            //{
            //    var img = ShellThumbnail.GetThumbnail(faf.FullName, size.Value.Width, size.Value.Height, ThumbnailOptions.None);
            //    faf.Icon = ConvertImageToString(img);
            //}

            return faf;
        }

        public static FileOrFolder GetFile(string fullName, Size? size = null)
        {
            size = size ?? _size;

            var inf = new FileInfo(fullName);

            var faf = new FileOrFolder
            {
                Name = inf.Name.EndsWith(inf.Extension) ?
                       inf.Name.Substring(0, inf.Name.LastIndexOf(inf.Extension)) :
                       inf.Name,
                FullName = inf.FullName,
                IsFolder = false,

                Extension = (inf.Extension.Length > 0 && inf.Extension[0] == '.') ?
                            inf.Extension.Substring(1) :
                            inf.Extension,
                Size = (ulong)inf.Length,
            };

            //faf.Icon = GetIcon(faf.FullName, size.Value);

            return faf;
        }

        public static string GetIcon(string name, Size size)
        {
            if (size.Width <= 0 || size.Height <= 0)
                return "";

            var res = "";

            if (File.Exists(name))
                if (size.Width <= 16 && size.Height <= 16)
                {
                    var icon = Icon.ExtractAssociatedIcon(name);
                    res = ConvertIconToString(icon);
                }
                else if (size.Width <= 32 && size.Height <= 32)
                {
                    var icon = Icon.ExtractAssociatedIcon(name);
                    res = ConvertIconToString(icon);
                }
                else
                {
                    res =
                        ConvertImageToString(GetReducedImage(name, size.Width, size.Height)) ??
                        ConvertIconToString(Icon.ExtractAssociatedIcon(name)) ??
                        "";
                }
            else if (Directory.Exists(name))
            {
                ;
            }

            return res;
        }

        public static bool ThumbnailCallback()
        {
            return false;
        }

        public static byte[] GetImageByte(string name, Size size)
        {
            return ConvertImageToByteArray(GetImage(name, size));
        }
        public static Image GetImage(string name, Size size)
        {
            if (!File.Exists(name) && !Directory.Exists(name))
                return GetImage(Environment.CurrentDirectory + "\\icon-none.png", size);

            return GetReducedImage(name, size.Width, size.Height);
        }

        public static Image GetReducedImage(string name, int Width, int Height)
        {
            try
            {
                Image ResourceImage = Image.FromFile(name);
                Image ReducedImage;

                Image.GetThumbnailImageAbort callb = new Image.GetThumbnailImageAbort(ThumbnailCallback);

                {
                    var deltaW = (float)ResourceImage.Width  / Width;
                    var deltaH = (float)ResourceImage.Height / Height;
                    if (deltaW > deltaH)
                        Height = (int)(ResourceImage.Height / deltaW);
                    else
                        Width  = (int)(ResourceImage.Width  / deltaH);
                }

                ReducedImage = ResourceImage.GetThumbnailImage(Width, Height, callb, IntPtr.Zero);

                return ReducedImage;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<FileOrFolder> GetChildFolders(string fullParentName, Size? size = null)
        {
            size = size ?? _size;

            var folders = new List<FileOrFolder>();
            try
            {
                var d = Directory.GetDirectories(fullParentName);
                foreach (var s in d)
                {
                    var dir = GetFolder(s, size);
                    if (dir != null)
                        folders.Add(dir);
                }

                var di = new DirectoryInfo(fullParentName);
                if (di.Parent != null)
                {
                    folders.Insert(0, GetFolder(di.Parent.FullName, size));
                    if (di.Parent.FullName == di.Root.FullName)
                        folders[0].Name = "[ root ]";
                    else
                    {
                        folders[0].Name = "[ " + folders[0].Name + " ]";

                        folders.Insert(0, GetFolder(di.Root.FullName, size));
                        folders[0].Name = "[ root ]";
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return folders;
        }

        public static List<FileOrFolder> GetChildFiles(string fullParentName, Size? size = null)
        {
            size = size ?? _size;

            var files = new List<FileOrFolder>();
                var f = Directory.GetFiles(fullParentName);
                foreach (var s in f)
                    files.Add(GetFile(s, size));
            return files;
        }

        public static FilesAndFolders GetChildFilesAndFolders(string fullParentName, Size? size = null)
        {
            size = size ?? _size;

                var files = Get(files:   GetChildFiles(fullParentName, size),
                                folders: GetChildFolders(fullParentName, size));
                return files;
       }

        public static string ConvertIconToString(Icon icon)
        {
            try
            {
                if (icon != null)
                    return ConvertImageToString(icon.ToBitmap());
            }
            catch (Exception)
            {
                // ignored
            }

            return null;
        }
        public static string ConvertImageToString(Image img)
        {
            if (img != null)
                try
                {
                    string buff;

                    using (var stream = new MemoryStream())
                    {
                        img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        buff = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(stream.ToArray()));
                    }

                    return buff;
                }
                catch (Exception)
                {
                    // ignored
                }

            return null;
        }
        public static byte[] ConvertImageToByteArray(Image img)
        {
            try
            {
                byte[] buff;

                using (var stream = new MemoryStream())
                {
                    img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                    buff = stream.ToArray();
                }

                return buff;
            }
            catch (Exception)
            {
                // ignored
            }

            return new byte[0];
        }
    }
}
