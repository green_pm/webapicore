﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApiDataEntity
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }

    public class TimeEntity : BaseEntity
    {
        [Required]
        public DateTime DateCreate { get; set; }
        [Required]
        public DateTime DateUpdate { get; set; }
    }
}