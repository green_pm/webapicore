﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace WebApiDataEntity
{
    public class WebApiDataContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid, IdentityUserClaim<Guid>,
                                     ApplicationUserRole, IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        public WebApiDataContext() : base() { }
        public WebApiDataContext(DbContextOptions<WebApiDataContext> options) : base(options)
        {
            // == Create tables ===
            Database.EnsureCreated();
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlite("Filename=Vs2015WinFormsEfcSqliteCodeFirst20170304Example.sqlite");
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>(b =>
            {
                b.HasKey(obj => obj.Id);
                b.HasMany(u => u.UserRoles)
                 .WithOne(ur => ur.User)
                 .HasForeignKey(ur => ur.UserId)
                 .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<ApplicationRole>(b =>
            {
                b.HasKey(obj=> obj.Id);
                b.HasMany(u => u.UserRoles)
                 .WithOne(ur => ur.Role)
                 .HasForeignKey(ur => ur.RoleId)
                 .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<ApplicationUserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired()
                    .OnDelete(DeleteBehavior.Cascade);

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired()
                    .OnDelete(DeleteBehavior.Cascade);

                // Maps to the AspNetUserRoles table
                // userRole.ToTable("AspNetUserRoles");
            });
            
            modelBuilder.Entity<IdentityUserLogin<Guid>>(entity =>
            {
                entity.HasKey(obj=> obj.UserId);
                entity.Property(m => m.LoginProvider)
                    .IsRequired()
                    .HasMaxLength(85);
                entity.Property(m => m.ProviderKey)
                    .IsRequired()
                    .HasMaxLength(85);
            });

            modelBuilder.Entity<IdentityUserToken<Guid>>(entity =>
            {
                entity.HasKey(obj=> obj.UserId);
                entity.Property(m => m.LoginProvider)
                    .IsRequired()
                    .HasMaxLength(85);
                entity.Property(m => m.Name)
                    .IsRequired()
                    .HasMaxLength(85);
            });
        }

        public DbSet<FolderEntity> Folders { get; set; }
    }
}