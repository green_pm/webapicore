﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApiDataEntity
{
    [Table("Folders")]
    public class FolderEntity : BaseEntity
    {
        [ForeignKey("Role")]
        public Guid RoleId { get; set; }
        public ApplicationRole Role { get; set; }

        public string Folder { get; set; }
    }
}