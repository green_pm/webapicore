﻿using Microsoft.Extensions.Logging;
using System;

namespace WebApiCore.Services
{
    public class LogService : ILogger
    {
        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException();
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            throw new NotImplementedException();
        }

        public void Log(string str)
        {
            try
            {
                CheckDirectory();
                var file = "logs\\" + DateTime.Now.ToString("yyyy.MM.dd") + ".log";
                System.IO.File.AppendAllText(file, str + Environment.NewLine);
                Console.WriteLine(str);
            }
            catch (Exception exc)
            {
                Console.WriteLine(str);
                Console.WriteLine(exc.Message);
                //throw;
            }
        }

        private void CheckDirectory()
        {
            if (!System.IO.Directory.Exists("logs"))
                System.IO.Directory.CreateDirectory("logs");
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            throw new NotImplementedException();
        }
    }
}
