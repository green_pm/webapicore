﻿using FilesAndFoldersWin;
//using FileExplorer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using WebApiCore.Models;
using WebApiDataEntity;

namespace WebApiCore.Services
{
    public interface IFilesService : IBaseService
    {
        void Ini(string rootDir);
        ResultModel GetFolders(FileParam param);
        List<FileOrFolder> GetFiles(FileParam param, ClaimsPrincipal User);
        Array GetIcons(FilesParam param);
        byte[] GetImage(FilesParam param);
        FileStream GetStream(string name, long firstByte);
    }

    public class FilesService : BaseService, IFilesService
    {
        private string _rootDir;
        private string rootDir {
            get { return _rootDir; }
            set {
                if (string.IsNullOrWhiteSpace(_rootDir))
                    _rootDir = value;
            }
        }
        private int strLen { get { return rootDir.Length; } }

        public FilesService(WebApiDataContext context, ILogger ls) : base(context, ls) { }

        public void Ini(string _rootDir)
        {
            rootDir = _rootDir;
        }

        public ResultModel GetFolders(FileParam param)
        {
            //if (!string.IsNullOrEmpty(param?.name))
            //{
            //    param.name = param.name.Base64ToString();
            //    if (string.IsNullOrEmpty(param?.name))
            //        ResultModel.GetErr("Входная строка имеет неверный формат!", "", "");
            //}

            if (param == null || string.IsNullOrEmpty(param.name) || param.name[0] != '\\')
                param = null;

            var name =  rootDir + param?.name;
            if (!(name?.IndexOf(rootDir) >= 0))
                name = rootDir;
            var files = FilesAndFolders.GetChildFolders(name, new Size())
                .Where(el => el.FullName.Length > rootDir.Length)
                .ToList();
            foreach (var el in files)
                el.FullName = el.FullName.Substring(strLen);

            if (!string.IsNullOrEmpty(param?.name))
            {
                files.Insert(0, new FileOrFolder
                {
                    IsFolder = true,
                    FullName = "",
                    Name = "[ root ]",
                    Extension = ""
                });
                var lastId = param.name.LastIndexOf('\\');
                if (lastId > 0)
                {
                    param.name = param.name.Substring(0, lastId);
                    lastId = param.name.LastIndexOf('\\');

                    files.Insert(1, new FileOrFolder
                    {
                        IsFolder = true,
                        FullName = param.name,
                        Name = "[ " + param.name.Substring(lastId + 1) + " ]",
                        Extension = ""
                    });
                }
            }

            //foreach (var item in files)
            //    if (!string.IsNullOrEmpty(item.FullName))
            //        item.FullName = item.FullName.Base64FromString();

            return ResultModel.GetOk(data: files);
        }

        public List<FileOrFolder> GetFiles(FileParam param, ClaimsPrincipal User)
        {
            if (!string.IsNullOrEmpty(param?.name))
            {
                param.name = param.name.Base64ToString();
                if (string.IsNullOrEmpty(param?.name))
                    ResultModel.GetErr("Входная строка имеет неверный формат!", "", "");
            }

            FilesAndFolders files;

            // грузим корневые папки
            if (string.IsNullOrEmpty(param?.name))
            {
                files = FilesAndFolders.Get(new List<FileOrFolder>(), new List<FileOrFolder>());

                var user = User.Identity.GetUser(db);
                var rols = user.UserRoles.Select(ur => ur.RoleId);
                var dirs = db.Folders
                    .AsNoTracking()
                    .Where(f => rols.Any(el => el == f.RoleId))
                    .GroupBy(dir => dir.Folder)
                    .Select(grp => new FolderEntity
                    {
                        Folder = grp.Max(gr => gr.Folder ?? ""),
                        Id     = grp.Max(gr => gr.Id),
                    })
                    .ToList();

                foreach (var dir in dirs)
                {
                    files.Folders.Add(new FileOrFolder() {
                        FullName = dir.Id.ToString().Base64FromString(),
                        IsFolder = true,
                        Name = dir.Folder.Substring(dir.Folder.LastIndexOf('\\') + 1)
                    });
                }
            }
            // грузим не корневые папки
            else
            {
                // если у роли пользователя нет доступа к запрашиваемой папке, то переадресация на корень
                if (!User.Identity.CheckUserFolder(param.name, db))
                    return GetFiles(null, User);

                var name = rootDir + param.name.RoleFolderToString(db);
                var dir = rootDir + param.name.RoleFolderOnly(db);
                var rolId = param.name.IndexOfAny(new[] {'\\', '/'});
                var rol = param.name;
                if (rolId > -1)
                    rol = rol.Substring(0, rolId);
                files = FilesAndFolders.GetChildFilesAndFolders(name, new Size());
                files.SummaryList.ForEach(el => el.FullName = el.FullName.Replace(dir, rol).Base64FromString());
            }

            if (!string.IsNullOrEmpty(param?.name))
            {
                files.Folders.Insert(0, new FileOrFolder
                {
                    IsFolder = true,
                    FullName = "",
                    Name = "[ в начало ]",
                 // Name = "[ root ]",
                    Extension = ""
                });
                var lastId = param.name.LastIndexOf('\\');
                if (lastId > 0)
                {
                    param.name = param.name.Substring(0, lastId);
                    var dir = param.name.RoleFolderOnly(db);
                    lastId = dir.LastIndexOf('\\');

                    files.Folders.Insert(1, new FileOrFolder
                    {
                        IsFolder = true,
                        FullName = param.name.Base64FromString(),
                        Name = "[ вверх ]",
                     // Name = "[ " + dir.Substring(lastId + 1) + " ]",
                        Extension = ""
                    });
                }
            }

            return files.SummaryList;
        }

        public Array GetIcons(FilesParam param)
        {
            var codes = param.names.ToList();
            param.Decode();
            var names = param.names.Select(el => rootDir + el.RoleFolderToString(db)).ToList();
            int size = param.size;
            bool compress = param.compress;

            if (names == null || names.Count < 1)
                return null;

            var _size = new Size(size, size);
            var icons = compress ?
                (System.Array)names.Select(name => new { name = codes[names.IndexOf(name)], data = Compress(FilesAndFolders.GetIcon(name, _size)) })
                                   .Select(ar => string.Join(", ", ar))
                                   .ToArray() :
                (System.Array)names.Select(name => new { name = codes[names.IndexOf(name)], data = FilesAndFolders.GetIcon(name, _size) })
                                   .ToArray();

            return icons;
        }

        public byte[] GetImage(FilesParam param)
        {
            param.Decode();
            var name = param.names.FirstOrDefault();

            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException();
            else name = rootDir + name.RoleFolderToString(db);
            int size = param.size;

            var _size = new Size(size, size);

            return FilesAndFolders.GetImageByte(name, _size);
        }

        public FileStream GetStream(string name, long firstByte)
        {
            int maxBytes = 1048576;
            name = name.Base64ToString();
            name = rootDir + name.RoleFolderToString(db);
            return new FileStream(name, FileMode.Open, FileAccess.Read, FileShare.Read, maxBytes * 50, true);
            //var stream = new FileStream(name, FileMode.Open, FileAccess.Read, FileShare.Read, maxBytes, FileOptions.Asynchronous);
            ////stream.Seek(firstByte, SeekOrigin.Begin);
            ////stream.SetLength(10000000);
            //return new FileStreamResult(stream, new MediaTypeHeaderValue("video/mp4").MediaType);
            //// new Microsoft.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream")
        }

        private List<int> Compress(string uncompressed)
        {
            // build the dictionary
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            for (int i = 0; i < 256; i++)
                dictionary.Add(((char)i).ToString(), i);

            string w = string.Empty;
            List<int> compressed = new List<int>();

            foreach (char c in uncompressed)
            {
                string wc = w + c;
                if (dictionary.ContainsKey(wc))
                {
                    w = wc;
                }
                else
                {
                    // write w to output
                    compressed.Add(dictionary[w]);
                    // wc is a new sequence; add it to the dictionary
                    dictionary.Add(wc, dictionary.Count);
                    w = c.ToString();
                }
            }

            // write remaining output if necessary
            if (!string.IsNullOrEmpty(w))
                compressed.Add(dictionary[w]);

            return compressed;
        }

        private string Decompress(List<int> compressed)
        {
            // build the dictionary
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            for (int i = 0; i < 256; i++)
                dictionary.Add(i, ((char)i).ToString());

            string w = dictionary[compressed[0]];
            compressed.RemoveAt(0);
            var decompressed = new StringBuilder(w);

            foreach (int k in compressed)
            {
                string entry = null;
                if (dictionary.ContainsKey(k))
                    entry = dictionary[k];
                else if (k == dictionary.Count)
                    entry = w + w[0];

                decompressed.Append(entry);

                // new sequence; add it to the dictionary
                dictionary.Add(dictionary.Count, w + entry[0]);

                w = entry;
            }

            return decompressed.ToString();
        }
    }

    public class FileParam
    {
        public string name { get; set; } = "";
    }

    public class FilesParam
    {
        public string[] names { get; set; } = new string[0];
        public int size { get; set; } = 0;
        public bool compress { get; set; } = false;

        public void Decode()
        {
            for (var i = 0; i < names.Length; i++)
                if (!string.IsNullOrEmpty(names[i]))
                    names[i] = names[i].Base64ToString();
        }

        public void Encode()
        {
            for (var i = 0; i < names.Length; i++)
                if (!string.IsNullOrEmpty(names[i]))
                    names[i] = names[i].Base64FromString();
        }
    }
}
