﻿using Microsoft.EntityFrameworkCore;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using WebApiDataEntity;

namespace WebApiCore.Services
{
    internal static class MyAppExt
    {
        public static ApplicationUser GetUser(this IIdentity identity, WebApiDataContext db)
        {
            if (!string.IsNullOrEmpty(identity?.Name))
                return db.Users
                    .Include(us => us.UserRoles)
                        .ThenInclude(ur => ur.Role)
                    .FirstOrDefault(us => us.UserName.CompareTo(identity.Name) == 0);

            if (identity is ClaimsIdentity)
            {
                var clIdn = identity as ClaimsIdentity;
                if (clIdn != null)
                {
                    var usId = clIdn.Claims.FirstOrDefault(cl => cl.Type.CompareTo(ClaimTypes.NameIdentifier) == 0)?.Value;
                    Guid usGd;
                    if (!string.IsNullOrEmpty(usId) && Guid.TryParse(usId, out usGd))
                        return db.Users
                            .Include(us => us.UserRoles)
                                .ThenInclude(ur => ur.Role)
                            .FirstOrDefault(us => us.Id.CompareTo(usGd) == 0);

                    var usName = clIdn.Claims.FirstOrDefault(cl => cl.Type.CompareTo(JwtRegisteredClaimNames.Sub) == 0)?.Value;
                    if (!string.IsNullOrEmpty(usName))
                        return db.Users
                            .Include(us => us.UserRoles)
                                .ThenInclude(ur => ur.Role)
                            .FirstOrDefault(us => us.UserName.CompareTo(usName) == 0);
                }
            }

            return null;
        }

        public static bool CheckUserFolder(this IIdentity identity, string name, WebApiDataContext db)
        {
            try
            {
                var user = identity.GetUser(db);
                var rolesId = user.UserRoles.Select(r => r.RoleId);

                var idId = name.IndexOf('\\');
                var id = idId > -1 ? name.Substring(0, idId) : name;

                Guid rlGd;
                if (Guid.TryParse(id, out rlGd))
                    return db.Folders.Any(f => rlGd == f.Id && rolesId.Contains(f.RoleId));
            }
            catch (Exception)
            {
            }

            return false;
        }

        public static string Base64FromString(this string name)
        {
            try
            {
                if (!string.IsNullOrEmpty(name))
                {
                    byte[] bytes = Encoding.UTF8.GetBytes(name);
                    name = Microsoft.AspNetCore.WebUtilities.Base64UrlTextEncoder.Encode(bytes);
                }

                return name;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static string Base64ToString(this string name)
        {
            try
            {
                if (!string.IsNullOrEmpty(name))
                {
                    byte[] bytes = Microsoft.AspNetCore.WebUtilities.Base64UrlTextEncoder.Decode(name);
                    name = Encoding.UTF8.GetString(bytes);
                }

                return name;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string RoleFolderOnly(this string name, WebApiDataContext db)
        {
            if (string.IsNullOrEmpty(name))
                return "";

            var idId = name.IndexOf('\\');
            var id = idId > -1 ? name.Substring(0, idId) : name;
            Guid rlGd;
            if (Guid.TryParse(id, out rlGd))
                return db.Folders.FirstOrDefault(f => f.Id == rlGd)?.Folder;

            return "";
        }
        public static string RoleFolderToString(this string name, WebApiDataContext db)
        {
            if (string.IsNullOrEmpty(name))
                return "";

            var dir = name.RoleFolderOnly(db);
            var idId = name.IndexOf('\\');
            return dir + (idId > -1 ? name.Substring(idId) : "");
        }
    }
}
