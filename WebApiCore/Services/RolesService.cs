﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using WebApiCore.Models;
using WebApiDataEntity;

namespace WebApiCore.Services
{
    public interface IRolesService : IBaseService
    {
        Task<ResultModel> GetRoles();
        Task<ResultModel> GetUserRoles();
        Task<ResultModel> GetRoleDirs();

        Task<ResultModel> AddRole(RoleModel model);
        Task<ResultModel> SetUserRoles(UserRolesModel model);
        Task<ResultModel> SetRoleDirs(RoleFoldersModel model);
    }

    public class RolesService : BaseService, IRolesService
    {
        private readonly RoleManager<ApplicationRole> _roleManager;

        public RolesService(
            RoleManager<ApplicationRole> roleManager,
            WebApiDataContext context,
            ILogger logService) : base(context, logService)
        {
            _roleManager = roleManager;
        }

        #region // GET //
        public async Task<ResultModel> GetRoles()
        {
            try
            {
                var res1 = await db.Roles.ToListAsync();
                var res2 = res1.Select(us => new RoleModel
                {
                    Id = us.Id,
                    Name = us.Name
                })
                .OrderBy(r => r.Name)
                .ToList();
                Log($"== GetRoles: {res2.Count} ==");
                return ResultModel.GetOk(data: res2);
            }
            catch (Exception exc)
            {
                return ResultModel.GetExc(exc.Message, exc.InnerException?.Message, exc);
            }
        }

        public async Task<ResultModel> GetUserRoles()
        {
            try
            {
                db.Roles.ToList();
                var res1 = await db.Users
                    .Include(us => us.UserRoles).ToListAsync();
                var res2 = res1.Select(us => new UserRolesModel
                {
                    UserId = us.Id,
                    UserName = us.UserName,
                    RolesId = us.UserRoles.Select(r => r.RoleId),
                    IsActive = us.EmailConfirmed
                })
                .OrderBy(u => u.UserName)
                .ToList();
                Log($"== GetUserRoles: {res2.Count} ==");
                return ResultModel.GetOk(data: res2);
            }
            catch (Exception exc)
            {
                return ResultModel.GetExc(exc.Message, exc.InnerException?.Message, exc);
            }
        }

        public async Task<ResultModel> GetRoleDirs()
        {
            try
            {
                var res = await db.Roles
                    .Select(r => new RoleFoldersModel
                    {
                        RoleId = r.Id,
                        RoleName = r.Name,
                        Folders = r.Folders.Select(f => f.Folder).OrderBy(f => f).ToList()
                    })
                    .OrderBy(r => r.RoleName)
                    .ToListAsync();
                Log($"== GetUserRoles: {res.Count} ==");
                return ResultModel.GetOk(data: res);
            }
            catch (Exception exc)
            {
                return ResultModel.GetExc(exc.Message, exc.InnerException?.Message, exc);
            }
        }
        #endregion

        #region // SET //
        public async Task<ResultModel> SetUserRoles(UserRolesModel model)
        {
            try
            {
                var user = await db.Users
                    .Include(us => us.UserRoles)
                    .FirstOrDefaultAsync(us => us.Id == model.UserId);
                if (user == null)
                    return ResultModel.GetErr("Пользователь с указанным идентификатором отсутствует. Возможно, изменились данные в БД или проблемы сети.", "", "");

                var add = model.RolesId.Where(rId => user.UserRoles.All(r => r.RoleId != rId))
                    .Select(rId => new ApplicationUserRole() {
                        RoleId = rId,
                        UserId = user.Id
                    });
                var del = user.UserRoles.Where(r => !model.RolesId.Contains(r.RoleId));

                db.UserRoles.RemoveRange(del);
                await db.UserRoles.AddRangeAsync(add);
                await db.SaveChangesAsync();

                // проверка сохранения
                db.Update(user);
                var check = user.UserRoles.Select(r => r.RoleId).ToList();
                if (check.Count == model.RolesId.Count() && check.All(ch => model.RolesId.Contains(ch)))
                    return ResultModel.GetOk("Сохранение прошло успешно.", "", "");
                else return ResultModel.GetErr("Сохранение прошло с ошибками! Обновите страницу и проверьте данные.", "", "");
            }
            catch (Exception exc)
            {
                return ResultModel.GetExc(exc.Message, exc.InnerException?.Message ?? exc.StackTrace, exc);
            }
        }

        public async Task<ResultModel> SetRoleDirs(RoleFoldersModel model)
        {
            try
            {
                var role = await db.Roles
                    .Include(rol => rol.Folders)
                    .FirstOrDefaultAsync(us => us.Id == model.RoleId);
                if (role == null)
                    return ResultModel.GetErr("Роль с указанным идентификатором отсутствует. Возможно, изменились данные в БД или проблемы сети.", "", "");

                var add = model.Folders.Where(fdr => role.Folders.All(f => f.Folder != fdr))
                    .Select(fdr => new FolderEntity()
                    {
                        RoleId = role.Id,
                        Folder = fdr
                    });
                var del = role.Folders.Where(r => !model.Folders.Contains(r.Folder));

                db.Folders.RemoveRange(del);
                await db.Folders.AddRangeAsync(add);
                await db.SaveChangesAsync();

                // проверка сохранения
                db.Update(role);
                var check = role.Folders.Select(r => r.Folder).ToList();
                if (check.Count == model.Folders.Count() && check.All(ch => model.Folders.Contains(ch)))
                    return ResultModel.GetOk("Сохранение прошло успешно.", "", "");
                else return ResultModel.GetErr("Сохранение прошло с ошибками! Обновите страницу и проверьте данные.", "", "");
            }
            catch (Exception exc)
            {
                return ResultModel.GetExc(exc.Message, exc.InnerException?.Message ?? exc.StackTrace, exc);
            }
        }

        public async Task<ResultModel> AddRole(RoleModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Name))
                    return ResultModel.GetErr("Имя роли не может быть пустым.", "Задайте другое имя.", "");
                if (await _roleManager.RoleExistsAsync(model.Name))
                    return ResultModel.GetErr("Роль с таким именем уже есть в списке.", "Задайте другое имя или используйте существующую роль.", "");

                var result = await _roleManager.CreateAsync(new ApplicationRole(model.Name));
                if (result.Succeeded)
                {
                    return ResultModel.GetOk("Добавление прошло успешно.", "", "");
                }
                else
                {
                    return ResultModel.GetErr("Сохранение прошло с ошибками! Обновите страницу и проверьте данные.",
                        string.Join(Environment.NewLine, result.Errors), "");
                }
            }
            catch (Exception exc)
            {
                return ResultModel.GetExc(exc.Message, exc.InnerException?.Message ?? exc.StackTrace, exc);
            }
        }

        public async Task<ResultModel> Delete(RoleModel model)
        {
            try
            {
                var role = await _roleManager.FindByNameAsync(model.Name);
                if (role != null)
                {
                    var res = await _roleManager.DeleteAsync(role);

                    if (res.Succeeded)
                        return ResultModel.GetOk("Удаление прошло успешно.", "", "");
                    else return ResultModel.GetErr("Удаление прошло с ошибками! Обновите страницу и повторите попытку.", "", "");
                }

                return ResultModel.GetErr("Роль не найдена! Обновите страницу и проверьте данные.", "", "");
            }
            catch(Exception exc)
            {
                return ResultModel.GetExc(exc.Message, exc.InnerException?.Message ?? exc.StackTrace, exc);
            }
        }
        #endregion
    }
}
