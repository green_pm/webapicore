﻿using Microsoft.Extensions.Logging;
using WebApiDataEntity;

namespace WebApiCore.Services
{
    public interface IBaseService
    {
    }

    public class BaseService : IBaseService
    {
        protected readonly WebApiDataContext db;
        protected readonly LogService ls;

        public BaseService(WebApiDataContext context, ILogger logService)
        {
            db = context;
            ls = logService as LogService;
        }

        protected void Log(string str)
        {
            ls.Log(str);
        }
    }
}
