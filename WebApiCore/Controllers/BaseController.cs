﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using WebApiCore.Models;
using WebApiCore.Services;

namespace WebApiCore.Controllers
{
    [Authorize]
    [ApiController]
    //[EnableCors("AllowAll")]
    [Route("[controller]/[action]")]
    public class BaseController<T> : ControllerBase where T : IBaseService
    {
        protected readonly T service;

        public BaseController(T _service)
        {
            service = _service;
        }

        public ActionResult GetOk <X>(string mes = "", string com = "", X data = null) where X : class => data is ResultModel ? Ok      (data) : Ok      (ResultModel.GetOk (mes, com, data));
        public ActionResult GetErr<X>(string mes = "", string com = "", X data = null) where X : class => data is ResultModel ? Conflict(data) : Conflict(ResultModel.GetErr(mes, com, data));
        public ActionResult GetExc<X>(string mes = "", string com = "", X data = null) where X : class => data is ResultModel ? Conflict(data) : Conflict(ResultModel.GetExc(mes, com, data));
        public ActionResult GetExc   (Exception exc) => GetExc(exc.Message, exc.InnerException?.Message ?? exc.StackTrace, exc);
    }
}