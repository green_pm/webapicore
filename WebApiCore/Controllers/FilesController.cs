﻿using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebApiCore.Services;

namespace WebApiCore.Controllers
{
    public class FilesController : BaseController<IFilesService>
    {
        public FilesController(IFilesService serv, IConfiguration Configuration) : base(serv)
        {
            serv.Ini(Configuration["RootDir"]);
        }

        [HttpPost]
        public ActionResult GetFiles([FromBody]FileParam param)
        {
            try
            {
                var res1 = service.GetFiles(param, User);
                var res2 = GetOk(data: res1);
                return res2;
            }
            catch (Exception exc)
            {
                return GetExc(exc);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult GetDirs([FromBody]FileParam root)
        {
            try
            {
                return GetOk(data: service.GetFolders(root));
            }
            catch (Exception exc)
            {
                return GetExc(exc);
            }
        }

        [HttpPost]
        public ActionResult GetIcons([FromBody]FilesParam param)
        {
            try
            {
                return GetOk(data: service.GetIcons(param));
            }
            catch (Exception exc)
            {
                return GetExc(exc);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetLinkImage(string name)
        {
            // Url.Action("GetLinkImage", "Files", new { name = @"\=Andrey=\Download\1535279032377.JPEG" });
            // /api/Files/GetLinkImage?name=%5C%3DAndrey%3D%5CDownload%5C1535279032377.JPEG

            if (string.IsNullOrEmpty(name))
                return GetExc(new ArgumentException("Неверный формат параметра"));

            try
            {
                var img = service.GetImage(new FilesParam()
                {
                    compress = false,
                    names = new[] { name },
                    size = 1920,
                });

                return File(img, "image/jpeg");
            }
            catch (Exception exc)
            {
                return GetExc(exc);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<FileStreamResult> GetVideo(string name, bool download = false)
        {
            try
            {
                var context = HttpContext;
                var response = context.Response;

                #region
                using (var fs = service.GetStream(name, 0))
                {
                    string range = context.Request.Headers["Range"];
                    long bytes_start = 0,
                         bytes_end = fs.Length;
                    if (range != null)
                    {
                        string[] range_info = range.Split(new char[] { '=', '-' });
                        bytes_start = Convert.ToInt64(range_info[1]);
                        if (!string.IsNullOrEmpty(range_info[2]))
                            bytes_end = Convert.ToInt64(range_info[2]);
                        if (bytes_start > 0)
                        {
                            response.StatusCode = 206;
                            fs.Seek(bytes_start, SeekOrigin.Begin);
                        }
                    }

                    response.Headers.Add("Accept-Ranges", "bytes");
                    response.ContentType = new MediaTypeHeaderValue("video/mp4").MediaType;
                    response.ContentLength = bytes_end - bytes_start;
                    response.Headers.Add("Content-Range", string.Format("bytes {0}-{1}/{2}", bytes_start, bytes_end - 1, fs.Length));

                    // setting this header tells the browser to download the file
                    if (download)
                        response.Headers.Add("content-disposition", "attachment; filename=" + Path.GetFileName(name));

                    await fs.CopyToAsync(response.Body);
                    return new FileStreamResult(response.Body, new MediaTypeHeaderValue("video/mp4").MediaType);
                }
                #endregion
            }
            catch (Exception)
            {
                throw;
                // return GetExc(exc);
            }
        }
    }
}