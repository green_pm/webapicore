﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApiCore.Services;

namespace WebApiCore.Controllers
{
    [AllowAnonymous]
    [ApiController]
    public class AccountController : BaseController<IAccountService>
    {
        public AccountController(IAccountService _service) : base(_service) { }

        [HttpPost]
        public async Task<ActionResult> Login([FromBody] LoginDto model)
        {
            try
            {
                return GetOk(data:  await service.Login(model));
            }
            catch (Exception exc)
            {
                return GetExc(exc.Message, exc.StackTrace, exc);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Register([FromBody] RegisterDto model)
        {
            try
            {
                return GetOk(data: await service.Register(model));
            }
            catch (Exception exc)
            {
                return GetExc(exc.Message, exc.StackTrace, exc);
            }
        }

        [HttpPost]
        public async Task<ActionResult> ConfirmEmail([FromBody] ConfirmDto conf)
        {
            try
            {
                return GetOk(data: await service.ConfirmEmail(conf.Param));
            }
            catch (Exception exc)
            {
                return GetExc(exc.Message, exc.StackTrace, exc);
            }
        }

        /// <summary>check current user is Authenticated</summary>
        [HttpGet]
        public async Task<bool> isAuth()
        {
            try
            {
                return await service.isAuth(User, Request.Headers["Authorization"]);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}