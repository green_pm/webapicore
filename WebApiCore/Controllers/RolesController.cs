﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApiCore.Models;
using WebApiCore.Services;

namespace WebApiCore.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RolesController : BaseController<IRolesService>
    {
        public RolesController(IRolesService serv) : base(serv) { }

        // GET

        [HttpGet]
        public async Task<ActionResult> GetRoles()
        {
            return GetOk(data: await service.GetRoles());
        }

        [HttpGet]
        public async Task<IActionResult> GetUserRoles()
        {
            return GetOk(data: await service.GetUserRoles());
        }

        [HttpGet]
        public async Task<IActionResult> GetRoleDirs()
        {
            return GetOk(data: await service.GetRoleDirs());
        }

        // SET

        [HttpPost]
        public async Task<IActionResult> AddRole(RoleModel model)
        {
            return GetOk(data: await service.AddRole(model));
        }

        [HttpPost]
        public async Task<IActionResult> SetUserRoles(UserRolesModel model)
        {
            return GetOk(data: await service.SetUserRoles(model));
        }

        [HttpPost]
        public async Task<IActionResult> SetRoleDirs(RoleFoldersModel model)
        {
            return GetOk(data: await service.SetRoleDirs(model));
        }
    }
}