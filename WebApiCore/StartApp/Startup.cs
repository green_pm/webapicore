﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiCore.Services;
using WebApiDataEntity;

namespace WebApiCore
{
    public class Startup
    {
        readonly string[] origins;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            origins = (Configuration["cors:rules:0:origin"] ?? "").Split(',');
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration["ConnectionStrings:DefaultConnection"]; // Configuration.GetConnectionString("DefaultConnection");

            // ===== Add our DbContext ========
            services.AddDbContext<WebApiDataContext>(options => options.UseMySQL(connection));
            // services.AddDbContext<WebApiDataContext>(options => options.UseSqlServer(connection));
            // services.AddDbContext<WebApiDataContext>(options => options.UseSqlite(connection));

            // ===== Add Identity ========
            services.AddIdentity<ApplicationUser, ApplicationRole>(options => {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
            })
                .AddEntityFrameworkStores<WebApiDataContext>()
                .AddDefaultTokenProviders();

            // ===== Add Jwt Authentication ========
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthorization(options =>
                {
                    options.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                        .RequireAuthenticatedUser()
                        .Build();
                })
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = true;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                });

            // ===== Add Routing ====
            services.AddRouting();

            //// ===== Add CORS =======
            //services.AddCors(b => b.AddPolicy("AllowAll", builder => {
            //    builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials();
            //    // builder.WithOrigins(origins).WithHeaders("accept", "content-type", "origin", "x-custom-header").AllowAnyHeader().AllowCredentials();
            //               }));

            // ===== Add Controllers ========
            services.AddControllers();

            services.AddSingleton<IConfiguration>(Configuration);
            
            services.AddSingleton<ILogger     , LogService    >();

            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IEmailService  , EmailService  >();
            services.AddScoped<IBaseService   , BaseService   >();
            services.AddScoped<IFilesService  , FilesService  >();
            services.AddScoped<IRolesService  , RolesService  >();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            WebApiDataContext dbContext,
            IServiceProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseExceptionHandler(errorApp =>
            {
            //    errorApp.Run(async context =>
            //    {
            //        //context.Response.StatusCode = 666; // or another Status accordingly to Exception Type
            //        //context.Response.ContentType = "application/json";
            //    });
            });

            //app.UseCors("AllowAll");
            app.UseCors(builder => builder
                .AllowCredentials()
                .WithOrigins(origins)
                .WithHeaders("authorization", "cache-control", "content-type")
                //.WithMethods("GET", "POST")
                .AllowAnyMethod()
                );

            app.UseRouting();

            //app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                name: "default",
                pattern: "{controller}/{action}");
            });

            // ===== Create tables ======
            //dbContext.Database.EnsureCreated();
            CreateUsersAndRolesAsync(provider, dbContext).Wait();
        }

        private async Task CreateUsersAndRolesAsync(IServiceProvider provider, WebApiDataContext dbContext)
        {
            //initializing custom roles 
            var RoleManager = provider.GetRequiredService<RoleManager<ApplicationRole>>();
            var UserManager = provider.GetRequiredService<UserManager<ApplicationUser>>();
            string[] roleNames = Configuration["UserSettings:UserRoles"].Split(',');
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    //create the roles and seed them to the database: Question 1
                    roleResult = await RoleManager.CreateAsync(new ApplicationRole(roleName));
                }
            }

            //Here you could create a super user who will maintain the web app
            var poweruser = new ApplicationUser
            {
                UserName = Configuration["UserSettings:UserName"],
                Email    = Configuration["UserSettings:UserEmail"],
                EmailConfirmed = true,
            };
            //Ensure you have these values in your appsettings.json file
            string userPWD = Configuration["UserSettings:UserPassword"];

            var _user = await UserManager.FindByEmailAsync(Configuration["UserSettings:UserRootAdmin"]);
            var _role = Configuration["UserSettings:UserRole"];
            if (_user == null)
            {
                var createPowerUser = await UserManager.CreateAsync(poweruser, userPWD);
                // проверка пароля на соответствие правилам
                // для дефолтного админа - отключено
                if (createPowerUser.Succeeded)
                {
                    //here we tie the new user to the role
                    await UserManager.AddToRoleAsync(poweruser, _role);
                }
            }
            else
            {
                var roles = await UserManager.GetRolesAsync(_user);
                if (!roles.Contains(_role))
                    await UserManager.AddToRoleAsync(_user, _role);
            }

            var admRole = dbContext.Roles.FirstOrDefault(el => el.Name == _role);
            if (dbContext.Folders.All(el => el.RoleId != admRole.Id)) {
                //dbContext.Folders.Add(new FolderEntity{Folder = null, RoleId = admRole.Id});
                //await dbContext.SaveChangesAsync();
            }
        }
    }

    public class ErrorDto
    {
        public int Code { get; set; }
        public string Message { get; set; }

        // other fields

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
