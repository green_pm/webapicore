﻿using System;
using System.Collections.Generic;

namespace WebApiCore.Models
{
    public class UserRolesModel
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public bool IsActive { get; set; }

        public IEnumerable<Guid> RolesId { get; set; }
    }

    public class RoleModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
