﻿using System;
using System.Collections.Generic;

namespace WebApiCore.Models
{
    public class RoleFoldersModel
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }

        public IEnumerable<string> Folders { get; set; }
    }
}
