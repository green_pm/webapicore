﻿using Microsoft.AspNetCore.Mvc;

namespace WebApiCore.Models
{
    public class ResultModel : ActionResult
    {
        public ResultType Type { get; set; }
        public string Message { get; set; }
        public string Comment { get; set; }

        public object Data { get; set; }

        public static ResultModel GetOk <X>(string mes = "", string com = "", X data = null) where X : class { return new ResultModel<X> { Type = ResultType.Ok , Message = mes, Comment = com, Data = data }; }
        public static ResultModel GetErr<X>(string mes = "", string com = "", X data = null) where X : class { return new ResultModel<X> { Type = ResultType.Err, Message = mes, Comment = com, Data = data }; }
        public static ResultModel GetExc<X>(string mes = "", string com = "", X data = null) where X : class { return new ResultModel<X> { Type = ResultType.Exc, Message = mes, Comment = com, Data = data }; }
    }

    public class ResultModel<T> : ResultModel where T : class
    {
        public new T Data
        {
            get { return base.Data as T; }
            set { base.Data = value; }
        }
    }

    public enum ResultType
    {
        Non,
        Ok,
        Err,
        Exc,
    }
}
