﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;

namespace FilesAndFoldersWin
{
    public class FileOrFolder
    {
        public string Name      { get; set; } = "" ;
        public string FullName  { get; set; } = "" ;
        public string Extension { get; set; } = "" ;
        public string Icon      { get; set; } = "" ;

        public bool?  IsFolder  { get; set; }

        public ulong? Size      { get; set; }
    }

    public class FilesAndFolders
    {
        public List<FileOrFolder> Files       { get; private set; }
        public List<FileOrFolder> Folders     { get; private set; }
        public List<FileOrFolder> SummaryList
        {
            get
            {
                var res = new List<FileOrFolder>();
                res.AddRange(Folders);
                res.AddRange(Files);
                return res;
            }
        }

        private static Size _size = new Size(32, 32);

        public static FilesAndFolders Get(List<FileOrFolder> files, List<FileOrFolder> folders)
        {
            return new FilesAndFolders() { Files = files, Folders = folders };
        }

        public static FileOrFolder GetFolder(string fullName, Size? size = null)
        {
            if (!Directory.Exists(fullName))
                return null;

            size = size ?? _size;

            var inf = new DirectoryInfo(fullName);

            //var perm = new FileIOPermission(FileIOPermissionAccess.Read, AccessControlActions.View, fullName);

            try
            {
                // ни одной рабочей проверки доступа к папкам не нашел,
                // потому проверяем таким костыльным стособом
                // вся проблема была с папкой System Volume Information
                inf.GetFileSystemInfos();
            }
            catch (Exception)
            {
                return null;
            }

            var faf = new FileOrFolder
            {
                Name     = inf.Name    ,
                FullName = inf.FullName,
                IsFolder = true,
            };

            #region // БАН // ранее иконки получал вместе с файлами //
            //if (size.Value.Width <= 16 && size.Value.Height <= 16)
            //{
            //    var icon = ShellIcon.GetSmallFolderIcon(faf.FullName);
            //    faf.Icon = ConvertIconToString(icon);
            //}
            //else if (size.Value.Width <= 32 && size.Value.Height <= 32)
            //{
            //    var icon = ShellIcon.GetLargeFolderIcon(faf.FullName);
            //    faf.Icon = ConvertIconToString(icon);
            //}
            //else
            //{
            //    var img = ShellThumbnail.GetThumbnail(faf.FullName, size.Value.Width, size.Value.Height, ThumbnailOptions.None);
            //    faf.Icon = ConvertImageToString(img);
            //}
            # endregion

            return faf;
        }

        public static FileOrFolder GetFile(string fullName, Size? size = null)
        {
            if (!File.Exists(fullName))
                return null;

            size = size ?? _size;

            var inf = new FileInfo(fullName);

            var faf = new FileOrFolder
            {
                Name = inf.Name.EndsWith(inf.Extension) ?
                       inf.Name.Substring(0, inf.Name.LastIndexOf(inf.Extension)) :
                       inf.Name,
                FullName = inf.FullName,
                IsFolder = false,

                Extension = (inf.Extension.Length > 0 && inf.Extension[0] == '.') ?
                            inf.Extension.Substring(1) :
                            inf.Extension,
                Size = (ulong) inf.Length,
            };

            // faf.Icon = GetIcon(faf.FullName, size.Value);

            return faf;
        }

        public static string GetIcon(string name, Size size)
        {
            //if (size.Width <= 0 || size.Height <= 0)
            //    return "";

            if (!File.Exists(name) && !Directory.Exists(name))
                return GetIcon(Environment.CurrentDirectory + "\\icon-none.png", size);

            var res = "";

            if (size.Width <= 16 && size.Height <= 16)
            {
                var icon = ShellIcon.GetSmallFileIcon(name);
                res = ConvertIconToBase64String(icon);
            }
            else if (size.Width <= 32 && size.Height <= 32)
            {
                var icon = ShellIcon.GetLargeFileIcon(name);
                res = ConvertIconToBase64String(icon);
            }
            else
            {
                var img = GetBufferedImage(name, size);
                res = ConvertImageToBase64String(img);
            }

            return res;
        }

        protected static string sysDir = "\\.sys";
        private static Image GetBufferedImage(string name, Size size)
        {
            try
            {
                var isFil = File.Exists(name);
                var isDir = isFil ? false : Directory.Exists(name);

                if (!isFil && !isDir)
                    return null;

                var dirOut = "";
                var filOut = "";
                if (isFil)
                {
                    var fInfo = new FileInfo(name);
                    dirOut = fInfo.DirectoryName + sysDir;
                    var fName = fInfo.Name.Substring(0, fInfo.Name.LastIndexOf('.'));
                    filOut = dirOut + $"\\{fName}_xW{size.Width}_xH{size.Height}.{fInfo.Extension}";
                }
                else
                {
                    dirOut = name + sysDir;
                    filOut = dirOut + $"\\dir_xW{size.Width}_xH{size.Height}.jpg";
                }

                if (!Directory.Exists(dirOut))
                {
                    var dir = Directory.CreateDirectory(dirOut);
                    dir.Attributes = FileAttributes.Hidden;
                }

                if (!File.Exists(filOut))
                {
                    var img = GetImage(name, size);
                    img.Save(filOut);
                    return img;
                }

                return File.Exists(filOut) ?
                    Image.FromFile(filOut) :
                    null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static byte[] GetImageByte(string name, Size size)
        {
            return ConvertImageToByteArray(GetImage(name, size));
        }
        public static Image GetImage(string name, Size size)
        {
            if (!File.Exists(name) && !Directory.Exists(name))
                return GetImage(Environment.CurrentDirectory + "\\icon-none.png", size);

            var img = ShellThumbnail.GetThumbnail(name, size.Width, size.Height, ThumbnailOptions.None);
            return img;
        }

        // ОООчень странно работает, нужно поэксперементировать
        //public static Image GetImage(string name, Size size)
        //{
        //    try
        //    {
        //        if (!File.Exists(name) && !Directory.Exists(name))
        //            return GetImage(Environment.CurrentDirectory + "\\icon-none.png", size);

        //        Image ResourceImage =
        //            File.Exists(name) ? Image.FromFile(name) :
        //            Directory.Exists(name) ? ShellThumbnail.GetThumbnail(name, size.Width, size.Height, ThumbnailOptions.None) :
        //            null;
        //        Image ReducedImage;

        //        Image.GetThumbnailImageAbort callb = new Image.GetThumbnailImageAbort(() => { return false; });

        //        ReducedImage = ResourceImage.GetThumbnailImage(size.Width, size.Height, callb, IntPtr.Zero);

        //        return ReducedImage;
        //    }
        //    catch (Exception exc)
        //    {
        //        throw exc;
        //    }
        //}

        public static List<FileOrFolder> GetChildFolders(string fullParentName, Size? size = null)
        {
            size = size ?? _size;

            var folders = new List<FileOrFolder>();
            try
            {
                if (!File.Exists(fullParentName) && !Directory.Exists(fullParentName))
                    return folders;

                var d = Directory.GetDirectories(fullParentName);
                foreach (var s in d)
                    if (!new DirectoryInfo(s).Attributes.HasFlag(FileAttributes.Hidden))
                    {
                        var dir = GetFolder(s, size);
                        if (dir != null)
                            folders.Add(dir);
                    }

                #region // добавляет реального родителя и корневой диск считываемой папки //
                //var di = new DirectoryInfo(fullParentName);
                //if (di.Parent != null)
                //{
                //    folders.Insert(0, GetFolder(di.Parent.FullName, size));
                //    if (di.Parent.FullName == di.Root.FullName)
                //        folders[0].Name = "[ root ]";
                //    else
                //    {
                //        folders[0].Name = "[ " + folders[0].Name + " ]";

                //        folders.Insert(0, GetFolder(di.Root.FullName, size));
                //        folders[0].Name = "[ root ]";
                //    }
                //}
                #endregion
            }
            catch (Exception)
            {
                throw;
            }
            return folders;
        }

        public static List<FileOrFolder> GetChildFiles(string fullParentName, Size? size = null)
        {
            size = size ?? _size;

            var files = new List<FileOrFolder>();
            try
            {
                if (!File.Exists(fullParentName) && !Directory.Exists(fullParentName))
                    return files;

                var f = Directory.GetFiles(fullParentName);
                foreach (var s in f)
                    if (!File.GetAttributes(s).HasFlag(FileAttributes.Hidden))
                        files.Add(GetFile(s, size));
            }
            catch (Exception)
            {
                //Response.Write("<script>window.alert('ku-ku');</script>");
                throw;
            }
            return files;
        }

        public static FilesAndFolders GetChildFilesAndFolders(string fullParentName, Size? size = null)
        {
            size = size ?? _size;

            try
            {
                var files = Get(files:   GetChildFiles(fullParentName, size),
                                folders: GetChildFolders(fullParentName, size));
                return files;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string ConvertIconToBase64String(Icon icon)
        {
            try
            {
                if (icon != null)
                    return ConvertImageToBase64String(icon.ToBitmap());
            }
            catch (Exception)
            {
                // ignored
            }

            return "";
        }
        public static string ConvertImageToBase64String(Image img)
        {
            try
            {
                string buff;

                using (var stream = new MemoryStream())
                {
                    img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                    buff = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(stream.ToArray()));
                }

                return buff;
            }
            catch (Exception)
            {
                // ignored
            }

            return "";
        }
        public static byte[] ConvertImageToByteArray(Image img)
        {
            try
            {
                byte[] buff;

                using (var stream = new MemoryStream())
                {
                    img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                    buff = stream.ToArray();
                }

                return buff;
            }
            catch (Exception)
            {
                // ignored
            }

            return new byte[0];
        }
    }
}
